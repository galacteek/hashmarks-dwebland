#!/usr/bin/env python3

from yarl import URL
from pathlib import Path
from omegaconf import OmegaConf
from datetime import datetime

import re
import ignition
import ipfshttpclient
import os.path
import subprocess

from urllib.parse import quote

from trimgmi import Document as GmiDocument
from trimgmi import LineType as GmiLineType

from gemgemgem import gempub

dstdir = Path('hashmarks/crawl')
p = dstdir.joinpath('gemini_crawling_results.yaml-ld')
images = dstdir.joinpath('images')

dstdir.mkdir(parents=True, exist_ok=True)
images.mkdir(parents=True, exist_ok=True)

data = OmegaConf.create({})
data['@context'] = 'ips://galacteek.ld/GemPubArchive'
data['@graph'] = []
client = ipfshttpclient.Client()


def download(url, dstf):
    response = ignition.request(str(url))

    dstf.write(response.data())


def gempub_analyze(url: URL):
    img_cid = None
    gp = gempub.get(url, ipfs_client=client)

    if not gp:
        return

    metadata = gp.m

    with gp.z() as zip:
        covername = metadata.get('cover')
        if covername:
            with zip.open(covername, 'r') as cover:
                imgdata = cover.read()

            basep = os.path.basename(covername)
            imgp = images.joinpath(f'{url.name}_{basep}')

            with open(imgp, 'wb') as fd:
                fd.write(imgdata)

            img_cid = client.add(str(imgp),
                                 cid_version=1, only_hash=True)['Hash']

    gp = {
        '@id': str(url),
        '@type': 'GemPubArchive'
    }

    for attr in ['title',
                 'author',
                 'description',
                 'published',
                 'publishDate',
                 'revisionDate',
                 'gpubVersion',
                 'version',
                 'language',
                 'charset',
                 'license',
                 'copyright']:
        val = metadata.get(attr)
        if val:
            gp[attr] = val

    if img_cid:
        gp['image'] = {
            '@id': f'ipfs://{img_cid}'
        }

    return gp


def search(q: str, page: int = 1, page_last: int = None):
    urls = []
    response = ignition.request(
        f'//geminispace.info/search/{page}?' + quote(q))

    if not response.success():
        return
    if response.is_a(ignition.ErrorResponse):
        return

    doc = GmiDocument()
    for line in response.data().split('\n'):
        doc.append(line)

    for line in doc.emit_line_objects(auto_tidy=True):
        if line.type == GmiLineType.LINK:
            urls.append(URL(line.extra))
        elif line.type == GmiLineType.REGULAR:
            ma = re.search('Page (\\d+) of (\\d+)', line.text)
            if ma:
                page_last = int(ma.group(1))

    for url in urls:
        name = url.name

        if name.endswith('.gpub'):
            obj = gempub_analyze(url)
            if obj:
                data['@graph'].append(obj)
                with open(p, 'w+t') as yld:
                    OmegaConf.save(config=data, f=yld.name)

    if not page_last or page <= page_last:
        search(q, page=page + 1, page_last=page_last)


def crawl():
    now = datetime.now()
    search('gpub')

    pp = Path('gemini/crawling/projects/')

    for project in pp.glob('*.yaml'):
        if project.is_dir():
            continue

        with open(project, 'rt') as y:
            cfg = OmegaConf.load(y)

        with gempub.create() as gp:
            gp.coverFrom(pp.joinpath(cfg.cover))
            gp.metadata.title = cfg.title
            gp.metadata.author = cfg.author
            gp.metadata.publishDate = now.strftime('%Y-%m-%d')

            for entry in cfg.entries:
                gp.pull(URL(entry.url), entry.title)

        dst = Path(f'{cfg.gpubname}_{gp.metadata.publishDate}.gpub')
        gp.write(dst)

        bookcid = client.add(str(dst), cid_version=1,
                             wrap_with_directory=True)[-1]['Hash']
        url = URL(f'ipfs://{bookcid}/{dst.name}')

        obj = gempub_analyze(url)

        if obj:
            data['@graph'].append(obj)
            pinn = f'{dst.name}-{gp.metadata.publishDate}'
            subprocess.Popen([
                'ipfs', 'pin', 'remote',
                'add', '--service=default',
                f'--name={pinn}',
                bookcid
            ]).wait()


crawl()

with open(p, 'w+t') as yld:
    OmegaConf.save(config=data, f=yld.name)


imcid = client.add(str(images), cid_version=1, recursive=True)[-1]['Hash']

pinn = 'gempub-images'
subprocess.Popen([
    'ipfs', 'pin', 'remote',
    'rm', '--service=default',
    f'--name={pinn}',
    '--force'
]).wait()

subprocess.Popen([
    'ipfs', 'pin', 'remote',
    'add', '--service=default',
    f'--name={pinn}',
    imcid
]).wait()
