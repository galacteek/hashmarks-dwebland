hashmarks (dwebland)
====================

*dwebland* is a collection of *hashmarks* (dweb bookmarks) definitions.
This is the default set of *hashmarks* used by galacteek.

Format docs
-----------

See [here](https://gitlab.com/galacteek/galacteek-ld-web4/-/blob/master/galacteek_ld_web4/contexts/galacteek.ld/Hashmark.yaml-ld)
for more details on the format of hashmarks.

Contributing
------------

Fork this repository and make a PR to the *master* branch. Thank you !
